# Forum Server

This program starts a server and runs a forum.

Users can perform the following actions:

- Register
- Log in
- Create posts
- Create comments
- Like and dislike posts and comments

Certain functionality requires users to be logged in.

## Running Locally

To run the server locally, follow these steps:

1. Migrate to the repository directory.
2. Open a terminal and type the following command:

```bash
    go run .
```

The server is now running on port 8080.

Open a web browser and go to localhost:8080.

## Running in a Docker Container
To run the server in a Docker container, execute the following commands:
```bash
docker build -t forum .
docker run -p 8080:8080 -it forum
```
Open a web browser and go to localhost:8080.