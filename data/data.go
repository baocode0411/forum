package data

type AllPosts struct {
	Posts        []Post
	Categories   []Topic
	UserName     string
	Loggedin     bool
	ErrorMessage string
}
type SinglePost struct {
	Post         Post
	Comments     []Comment
	ErrorMessage string
	LoggedIn     bool
}

type Comment struct {
	Id           int
	Content      string
	Creator      string
	CreatorId    int
	PostId       int
	Likes        int
	Dislikes     int
	CreationDate string
}

type User struct {
	Id             int
	Email          string
	UserName       string
	Joined         string
	Password       string
	Session        string
	NumberComments int
	NumberPosts    int
}

type Topic struct {
	Id    int
	Topic string
}

type Post struct {
	PostId   int
	UserId   int
	TopicId  int
	Creator  string
	Topic    string
	Label    string
	Content  string
	Created  string
	Likes    int
	Dislikes int
}

type Alert struct {
	AlertMessage string
	AlertCode    int
	AllUserPosts []Post
	AllReactions []Post
	LoggedIn     bool
}

var AlertStruct = Alert{}