package sql

import (
	"fmt"
	"forum/data"
	"strconv"
	"strings"
)

/* Adds a user
 * @param u - The user to add
 */
func AddUser(u data.User) {
	_, err := DB.Exec(addData(userT, "userName,email,pass,joined,sess", "?,?,?,?,?"), u.UserName, u.Email, u.Password, u.Joined, u.Session)

	if err != nil {
		fmt.Println("Could not insert into "+userT+"table: ", err.Error())
	}
}

/* Adds a topic
* @param topic - The topic to add
 */
func AddTopic(topic string) {
	_, err := DB.Exec(addData(topicT, "topic", "?"), topic)

	if err != nil {
		fmt.Println("Could not insert into "+topicT+"table: ", err.Error())
	}
}

/* Adds a post
* @param p - The post data
 */
func AddPost(p data.Post) {
	_, err := DB.Exec(addData(postT, "userId,topicId,topic,author,label,content,created", "?,?,?,?,?,?,?"), p.UserId, p.TopicId, p.Topic, p.Creator, p.Label, p.Content, p.Created)

	if err != nil {
		fmt.Println("Could not insert into posts table: ", err.Error())
	}
}

/* Adds a comment
* @param c - The comment data
 */
func AddComment(c data.Comment) {
	_, err := DB.Exec(addData(commentT, "userId,postId,content,created", "?,?,?,?"), c.CreatorId, c.PostId, c.Content, c.CreationDate)

	if err != nil {
		fmt.Println("Could not insert into comments table: ", err.Error())
	}
}

/* Gets a user
* @param val - The value to be checked
* @param name - If the value is user name / email or session
* @returns *data.User
 */
func GetUser(val string, ty string) *data.User {
	sel := `id,
			userName,
			email,
			pass,
			joined,
			sess`

	where := ""

	if ty == "name" {
		where = "userName LIKE ?"
	} else if ty == "email" {
		where = "email LIKE ?"
	} else if ty == "session" || ty == "sess" {
		where = "sess LIKE ?"
	}
	row := DB.QueryRow(getRow(userT, sel, where, "u"), val)

	ret := &data.User{}

	err := row.Scan(&ret.Id, &ret.UserName, &ret.Email, &ret.Password, &ret.Joined, &ret.Session)

	if err != nil {
		fmt.Println("Error: could not get " + strings.TrimSuffix(userT, "s") + "\n" + err.Error())
		return nil
	}

	return ret
}

/* Gets a topic
 * @param val - The value to be checked
 * @param ty - If the value is id or topic
 * @returns *data.Topic
 */
func GetTopic(val any, ty string) *data.Topic {
	where := ""

	if ty == "id" {
		where = "id == ?"
	} else if ty == "topic" {
		where = "topic LIKE ?"
	}

	row := DB.QueryRow(getRow(topicT, "*", where, "t"), val)

	ret := &data.Topic{}

	err := row.Scan(&ret.Id, &ret.Topic)

	if err != nil {
		//fmt.Println("Error: could not get " + strings.TrimSuffix(topicT, "s") + "\n" + err.Error())
		return nil
	}
	return ret
}

func GetSinglePostFromId(id int) *data.Post {
	sel := `id,
			topicId,
			userId,
			(SELECT userName FROM ` + userT + ` AS u WHERE u.id == p.userId) AS Creator,
			topic,
			label,
			content,
			created,
			(SELECT COUNT(r.id) FROM ` + postRaT + ` AS r WHERE r.postId == p.id AND rating LIKE 'likes') AS Likes,
			(SELECT COUNT(r.id) FROM ` + postRaT + ` AS r WHERE r.postId == p.id AND rating LIKE 'dislikes') AS Dislikes`
	where := "id == ?"

	row := DB.QueryRow(getRow(postT, sel, where, "p"), id)
	post := &data.Post{}

	err := row.Scan(&post.PostId, &post.TopicId, &post.UserId, &post.Creator, &post.Topic, &post.Label, &post.Content, &post.Created, &post.Likes, &post.Dislikes)

	if err != nil {
		fmt.Println("Could not get post:", err.Error())
		return nil
	}
	return post
}

func GetComment(postId int, commentId int) data.Comment {
	sel := `id,
			(SELECT u.userName FROM ` + userT + ` AS u WHERE u.id == c.userId) AS Creator,
			userId,
			postId,
			content,
			created,
			(SELECT COUNT(r.id) FROM ` + commentRaT + ` AS r WHERE r.commentId == c.id AND rating LIKE 'likes') AS Likes,
			(SELECT COUNT(r.id) FROM ` + commentRaT + ` AS r WHERE r.commentId == c.id AND rating LIKE 'dislikes') AS Dislikes`
	where := "c.id == ? AND c.postId == ?"

	row := DB.QueryRow(getRow(commentT, sel, where, "c"), commentId, postId)

	comment := data.Comment{}

	err := row.Scan(&comment.Id, &comment.Creator, &comment.CreatorId, &comment.PostId, &comment.Content, &comment.CreationDate, &comment.Likes, &comment.Dislikes)

	if err != nil {
		fmt.Println("Could not get single comment ", err.Error())
		return data.Comment{}
	}

	return comment
}

func GetCommentsForPost(id int) []data.Comment {
	var comments []data.Comment
	sel := `id,
			(SELECT u.userName FROM ` + userT + ` AS u WHERE u.id == c.userId) AS Creator,
			userId,
			postId,
			content,
			created,
			(SELECT COUNT(r.id) FROM ` + commentRaT + ` AS r WHERE r.commentId == c.id AND rating LIKE 'likes') AS Likes,
			(SELECT COUNT(r.id) FROM ` + commentRaT + ` AS r WHERE r.commentId == c.id AND rating LIKE 'dislikes') AS Dislikes`
	where := "c.postId == ?"
	sort := ""

	rows, err := DB.Query(getRows(commentT, sel, where, "c", sort), id)

	if err != nil {
		fmt.Println("Could not get comments for posts:", err.Error())
		return comments
	}
	for rows.Next() {
		comment := data.Comment{}

		err := rows.Scan(&comment.Id, &comment.Creator, &comment.CreatorId, &comment.PostId, &comment.Content, &comment.CreationDate, &comment.Likes, &comment.Dislikes)

		if err != nil {
			fmt.Println("Could not get comments for posts loop:", err.Error())
			break
		}

		comments = append(comments, comment)
	}

	return comments
}

func GetAllPosts() []data.Post {
	var allposts []data.Post
	sel := `id,
			topicId,
			userId,
			(SELECT userName FROM ` + userT + ` AS u WHERE u.id == p.userId) AS Creator,
			topic,
			label,
			content,
			created,
			(SELECT COUNT(r.id) FROM ` + postRaT + ` AS r WHERE r.postId == p.id AND rating LIKE 'likes') AS Likes,
			(SELECT COUNT(r.id) FROM ` + postRaT + ` AS r WHERE r.postId == p.id AND rating LIKE 'dislikes') AS Dislikes`

	rows, err := DB.Query(getRows(postT, sel, "", "p", ""))

	if err != nil {
		fmt.Println("Could not get posts:", err.Error())
		return allposts
	}

	for rows.Next() {
		post := data.Post{}

		err := rows.Scan(&post.PostId, &post.TopicId, &post.UserId, &post.Creator, &post.Topic, &post.Label, &post.Content, &post.Created, &post.Likes, &post.Dislikes)

		if err != nil {
			fmt.Println("Could not get posts loop:", err.Error())
			break
		}

		allposts = append(allposts, post)
	}
	return allposts

}

func GetUserPosts(userId int) []data.Post {
	var allposts []data.Post
	sel := `id,
			topicId,
			userId,
			(SELECT userName FROM ` + userT + ` AS u WHERE u.id == p.userId) AS Creator,
			topic,
			label,
			content,
			created,
			(SELECT COUNT(r.id) FROM ` + postRaT + ` AS r WHERE r.postId == p.id AND rating LIKE 'likes') AS Likes,
			(SELECT COUNT(r.id) FROM ` + postRaT + ` AS r WHERE r.postId == p.id AND rating LIKE 'dislikes') AS Dislikes`

	rows, err := DB.Query(getRows(postT, sel, "p.userId == ?", "p", ""), userId)

	if err != nil {
		fmt.Println("Could not get user posts:", err.Error())
		return allposts
	}

	for rows.Next() {
		post := data.Post{}

		err := rows.Scan(&post.PostId, &post.TopicId, &post.UserId, &post.Creator, &post.Topic, &post.Label, &post.Content, &post.Created, &post.Likes, &post.Dislikes)

		if err != nil {
			fmt.Println("Could not get user posts loop:", err.Error())
			break
		}

		allposts = append(allposts, post)
	}
	return allposts
}

func GetUserLikedPosts(userId int) []data.Post {
	var allposts []data.Post
	sel := `postId,
			(SELECT p.topicId FROM ` + postT + ` AS p WHERE p.id == pR.postId) AS topicId,
			(SELECT p.userId FROM ` + postT + ` AS p WHERE p.id == pR.postId) AS userId,
			(SELECT userName FROM ` + userT + ` AS u WHERE u.id == pR.userId) AS Creator,
			(SELECT p.topic FROM ` + postT + ` AS p WHERE p.id == pR.postId) AS Topic,
			(SELECT p.label FROM ` + postT + ` AS p WHERE p.id == pR.postId) AS label,
			(SELECT p.content FROM ` + postT + ` AS p WHERE p.id == pR.postId) AS content,
			(SELECT p.created FROM ` + postT + ` AS p WHERE p.id == pR.postId) AS created,
			(SELECT COUNT(r.id) FROM ` + postRaT + ` AS r WHERE r.postId == pR.postId AND rating LIKE 'likes') AS Likes,
			(SELECT COUNT(r.id) FROM ` + postRaT + ` AS r WHERE r.postId == pR.postID AND rating LIKE 'dislikes') AS Dislikes`

	rows, err := DB.Query(getRows(postRaT, sel, "pR.userId == ? AND pR.rating LIKE 'likes'", "pR", ""), userId)

	if err != nil {
		fmt.Println("Could not get user posts:", err.Error())
		return allposts
	}

	for rows.Next() {
		post := data.Post{}

		err := rows.Scan(&post.PostId, &post.TopicId, &post.UserId, &post.Creator, &post.Topic, &post.Label, &post.Content, &post.Created, &post.Likes, &post.Dislikes)

		if err != nil {
			fmt.Println("Could not get user posts loop:", err.Error())
			break
		}

		allposts = append(allposts, post)
	}
	return allposts
}

func GetAllCategories() []data.Topic {
	var allCategories []data.Topic

	rows, err := DB.Query(getRows(topicT, "*", "", "", ""))

	if err != nil {
		fmt.Println("Could not get topics:", err.Error())
		return allCategories
	}

	for rows.Next() {
		category := data.Topic{}

		err := rows.Scan(&category.Id, &category.Topic)

		if err != nil {
			fmt.Println("Could not get topics loop:", err.Error())
			break
		}

		allCategories = append(allCategories, category)
	}
	return allCategories
}

/* Updates user data
 * @param nD - The new data
 * @param sess - If session should uppdate
 */
func UpdateUser(nD data.User, sess bool) {
	if len(nD.UserName) > 0 {
		_, err := DB.Exec(updateData(userT, "userName = ?", "id == ?"), nD.UserName, nD.Id)

		if err != nil {
			fmt.Println("Could not get update user:", err.Error())
		}
	}

	if len(nD.Email) > 0 {
		_, err := DB.Exec(updateData(userT, "email = ?", "id == ?"), nD.Email, nD.Id)

		if err != nil {
			fmt.Println("Could not get update user:", err.Error())
		}
	}

	if len(nD.Password) > 0 {
		_, err := DB.Exec(updateData(userT, "pass = ?", "id == ?"), nD.Password, nD.Id)

		if err != nil {
			fmt.Println("Could not get update user:", err.Error())
		}
	}

	if sess {
		_, err := DB.Exec(updateData(userT, "sess = ?", "id == ?"), nD.Session, nD.Id)

		if err != nil {
			fmt.Println("Could not get update user:", err.Error())
		}
	}

}

func CheckRatingExist(table, idType string, id int, userId int) (bool, string) {
	rating := ""

	err := DB.QueryRow(getRow(table, "rating", "userId = ? AND "+idType+" = ?", ""), userId, id).Scan(&rating)

	return err == nil, rating
}

// rating 0 == comments_ratings || posts_ratings
// rating 1 == likes || dislikes
// rating 2 == postId || commentId
func AddRating(ratingSlice []string, userId int, commentId *int) string {
	table := ratingSlice[0]
	rating := ratingSlice[1]
	postOrCommentId, err := strconv.Atoi(ratingSlice[2])

	if err != nil {
		return "Rating id is not number HAHAHA"
	}

	idType := ""
	if table == commentRaT {
		idType = "commentId"
		*commentId = postOrCommentId
	} else {
		idType = "postId"
	}

	ratingExist, currentRating := CheckRatingExist(table, idType, postOrCommentId, userId)

	if ratingExist && currentRating != rating {
		_, err := DB.Exec(updateData(table, "rating = ?", idType+" = ? AND userId = ?"), rating, postOrCommentId, userId)

		if err != nil {
			fmt.Println("rating update error", err)
			return "rating update error"
		}

	} else if !ratingExist {
		_, err := DB.Exec(addData(table, "userId,"+idType+",rating,bonusRating", "?,?,?,?"), userId, postOrCommentId, rating, "none")

		if err != nil {
			fmt.Println("rating adding error", err)
			return "rating adding error"
		}
	}
	return ""
}
