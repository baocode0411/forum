package sql

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"strconv"
)

const (
	dbFile     string = "./DataBase/Based.db"
	dbVersion  int    = 1
	userT      string = "users"
	topicT     string = "topics"
	postT      string = "posts"
	commentT   string = "comments"
	postRaT    string = "posts_ratings"
	commentRaT string = "comments_ratings"
	lowHigh    string = "ASC"
	highLow    string = "DESC"
)

var DB *sql.DB

func open() *sql.DB {
	ret, err := sql.Open("sqlite3", dbFile)
	if err != nil {
		log.Fatalln("Could not open database:", err.Error())
	}
	return ret
}

func checkandAddTopics() {
	topics := []string{"SPORTS", "COOKING", "CODING", "POLITICS", "ADULT", "ART", "SCIENCE and TECHNOLOGY", "SOCIAL", "OFFTOPIC"}

	for _, topic := range topics {
		t := GetTopic(topic, "topic")

		if t == nil {
			AddTopic(topic)
		}
	}
}

func createTables() {
	var err error
	//Create user table
	_, err = DB.Exec(`CREATE TABLE IF NOT EXISTS ` + userT + ` (
		id INTEGER PRIMARY KEY AUTOINCREMENT,
		userName TEXT NOT NULL,
		email TEXT NOT NULL,
		pass TEXT NOT NULL,
		joined TEXT NOT NULL,
		sess TEXT DEFAULT ''
	);`)
	if err != nil {
		log.Fatalln("Could not create database table "+userT+":", err.Error())
	}
	//Create topic table
	_, err = DB.Exec(`CREATE TABLE IF NOT EXISTS topics (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	topic TEXT NOT NULL
	);`)
	if err != nil {
		log.Fatalln("Could not create database table "+topicT+":", err.Error())
	}

	//Create post table
	_, err = DB.Exec(`CREATE TABLE IF NOT EXISTS ` + postT + ` (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	userId INTEGER NOT NULL,
	topicId INTEGER NOT NULL,
	topic TEXT NOT NULL,
	author TEXT NOT NULL,
	label TEXT NOT NULL,
	content TEXT NOT NULL,
	created TEXT NOT NULL,
	FOREIGN KEY (userId) REFERENCES users(id) ON DELETE CASCADE, 
	FOREIGN KEY (topicId) REFERENCES topics(id) ON DELETE CASCADE 
	);`)
	if err != nil {
		log.Fatalln("Could not create database table "+commentT+":", err.Error())
	}
	//Create comment table
	_, err = DB.Exec(`CREATE TABLE IF NOT EXISTS ` + commentT + ` (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	userId INTEGER NOT NULL,
	postId INTEGER NOT NULL,
	content TEXT NOT NULL,
	created TEXT NOT NULL,
	FOREIGN KEY (userId) REFERENCES users(id) ON DELETE CASCADE,
	FOREIGN KEY (postId) REFERENCES posts(id) ON DELETE CASCADE 
	);`)
	if err != nil {
		log.Fatalln("Could not create database table "+commentT+":", err.Error())
	}
	//Create Post likes/dislikes table
	_, err = DB.Exec(`CREATE TABLE IF NOT EXISTS ` + postRaT + ` (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	userId INTEGER NOT NULL,
	postId INTEGER NOT NULL,
	rating TEXT NOT NULL,
	bonusRating TEXT DEFAULT '',
	FOREIGN KEY (userId) REFERENCES users(id) ON DELETE CASCADE,
	FOREIGN KEY (postId) REFERENCES posts(id) ON DELETE CASCADE
	);`)
	if err != nil {
		log.Fatalln("Could not create database table "+postRaT+":", err.Error())
	}
	//Create Comment likes/dislikes table
	_, err = DB.Exec(`CREATE TABLE IF NOT EXISTS ` + commentRaT + ` (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	userId INTEGER NOT NULL,
	commentId INTEGER NOT NULL,
	rating TEXT NOT NULL,
	bonusRating TEXT DEFAULT '',
	FOREIGN KEY (userId) REFERENCES users(id) ON DELETE CASCADE,
	FOREIGN KEY (commentId) REFERENCES comments(id) ON DELETE CASCADE
	);`)
	if err != nil {
		log.Fatalln("Could not create database table "+commentRaT+":", err.Error())
	}
}

// Removes all the tables
func removeTables() {
	DB.Exec("DROP IF EXISTS " + commentRaT + ";")
	DB.Exec("DROP IF EXISTS " + postRaT + ";")
	DB.Exec("DROP IF EXISTS " + commentT + ";")
	DB.Exec("DROP IF EXISTS " + postT + ";")
	DB.Exec("DROP IF EXISTS " + topicT + ";")
	DB.Exec("DROP IF EXISTS " + userT + ";")
}

/* Gets the database internal version number
* @returns int
 */
func getVersion() int {
	row := DB.QueryRow("PRAGMA user_version;")
	if row == nil {
		fmt.Println("Error: Could not get version from database")
	}
	v := -1
	row.Scan(&v)
	return v
}

/* Gets the database internal version number
* @param v - The version number
 */
func updateVersion(v int) {
	_, err := DB.Exec("PRAGMA user_version = " + strconv.Itoa(v) + ";")
	if err != nil {
		fmt.Println("Version error:", err.Error())
	}
}

// Initialize the database
func InitDatabase() {
	if _, err := os.Stat(dbFile); err != nil {
		f, e := os.Create(dbFile)
		if e != nil {
			log.Fatalln("Could not create database:", err.Error())
		}
		f.Close()
	}
	DB = open()

	v := getVersion()
	if v == 0 {
		updateVersion(dbVersion)
	} else if v != 0 && v != dbVersion {
		removeTables()
		updateVersion(dbVersion)
	}
	createTables()
	checkandAddTopics()
}

/* Adds data to the database
* @param table - The table
* @param columns - The columns the data represents
* @param values - Values to be assigned to the columns
* @param where - What to select
 */
func addData(table string, columns string, values string) string {
	return "INSERT INTO " + table + "(" + columns + ") VALUES(" + values + ");"
}

/* Gets single row from the database
* @param table - The table
* @param sel - Columns to select
* @param where - What to select
 */
func getRow(table string, sel string, where string, as string) string {
	if len(as) < 1 {
		return "SELECT " + sel + " FROM " + table + " WHERE " + where + ";"
	}
	return "SELECT " + sel + " FROM " + table + " AS " + as + " WHERE " + where + ";"
}

/* Gets multiple rows from the database
* @param table - The table
* @param sel - Columns to select
* @param where - What to select
 */
func getRows(table string, sel string, where string, as string, sort string) string {
	q := ""

	if len(as) < 1 {
		q = "SELECT " + sel + " FROM " + table
	} else if len(as) > 0 {
		q = "SELECT " + sel + " FROM " + table + " As " + as
	}

	if len(where) > 0 {
		q = q + " WHERE " + where
	}

	if len(sort) > 0 {
		q = q + " OREDER BY " + sort
	}

	return q + ";"
}

/* Updates data in the database
* @param table - The table
* @param set - What to update
* @param where - Where to update
 */
func updateData(table string, set string, where string) string {
	return "UPDATE " + table + " SET " + set + " WHERE " + where + ";"
}