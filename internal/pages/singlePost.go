package pages

import (
	//"forum/data"
	//"forum/internal/sql"
	"fmt"
	"forum/data"
	"forum/internal/sql"
	"forum/internal/utility"
	"net/http"
	"strconv"
	"strings"
	"time"
)

func SinglePost(w http.ResponseWriter, r *http.Request) {
	url := r.URL.Path
	urlSlice := strings.Split(url, "/")
	postId, err := strconv.Atoi(urlSlice[2])

	if err != nil {
		ErrorPg(w, 404)
		return
	}

	p := sql.GetSinglePostFromId(postId)

	//TODO make this in regex instead of this shit
	if len(url) < 11 || url[:11] != "/all_posts/" || len(urlSlice) != 3 || p == nil {
		ErrorPg(w, 404)
		return
	}

	var singlePostPage data.SinglePost
	singlePostPage.LoggedIn = false
	if utility.CheckCookieExist(w, r) {
		singlePostPage.LoggedIn = true

	}
	singlePostPage.Post = *p
	singlePostPage.Comments = sql.GetCommentsForPost(postId)
	if len(singlePostPage.Comments) == 0 {
		singlePostPage.ErrorMessage = "no comments"
	}

	if r.Method == "POST" {
		err = r.ParseForm()
		if err != nil {
			fmt.Println(err)
		}
		u, err := utility.GetUserIfLoggedIn(w, r)

		if err != nil {
			singlePostPage.ErrorMessage = err.Error()
			ErrorPg(w, 400)
			return
		} else {
			fmt.Println(r.Form)
			comment := r.Form.Get("comment")
			rating := r.Form.Get("rating")
			fmt.Println("formvalue rating;", rating)
			fmt.Println("formvalue comment;", comment)
			if !utility.CheckCookieExist(w, r) {
				singlePostPage.ErrorMessage = "you need to be logged in dumbass"
			} else if rating != "" {
				fmt.Println("got rating;", rating)
				ratingS := strings.Split(rating, "-")
				err := ""
				//TODO regex check to see if value from FormValue is correct
				if ratingS[0] == "comments_ratings" {
					commentId := -1
					err = sql.AddRating(ratingS, u.Id, &commentId)
					singlePostPage.Comments[commentId-1] = sql.GetComment(singlePostPage.Post.PostId, commentId)
				} else if ratingS[0] == "posts_ratings" {
					err = sql.AddRating(ratingS, u.Id, nil)
					singlePostPage.Post = *sql.GetSinglePostFromId(singlePostPage.Post.PostId)
				}

				if err != "" {
					//TODO ERROR?
					ErrorPg(w, 500)
				}

			} else if comment != "" {
				foundComment := utility.CheckInfoCorrect(comment)
				if comment == "" || !foundComment {
					singlePostPage.ErrorMessage = "Comment has no data"
				}
				if singlePostPage.ErrorMessage == "" || singlePostPage.ErrorMessage == "no comments" {
					userComment := data.Comment{}
					userComment.CreatorId = u.Id
					userComment.Content = comment
					userComment.Likes = 0
					userComment.Dislikes = 0
					userComment.PostId = postId
					userComment.CreationDate = time.Now().Format("2006-01-02 15:04:05")
					sql.AddComment(userComment)
					singlePostPage.Comments = sql.GetCommentsForPost(singlePostPage.Post.PostId)
				} else {
					fmt.Println("comment failed :(", singlePostPage.ErrorMessage)
				}
			}
		}
	}
	if singlePostPage.LoggedIn {
		err := Tmpl.ExecuteTemplate(w, "header_loggedin.html", nil)
		if err != nil {
			ErrorPg(w, 500)
		}
	} else {
		err := Tmpl.ExecuteTemplate(w, "header.html", nil)
		if err != nil {
			ErrorPg(w, 500)
		}

	}
	Tmpl.ExecuteTemplate(w, "singlePost.html", singlePostPage)
}
