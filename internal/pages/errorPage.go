package pages

import (
	"fmt"
	"io"
)

type ErrorMsg struct {
	ErrorMessage string
	ErrorNum     int
}

var msg = map[int]ErrorMsg{
	400: {
		ErrorNum:     400,
		ErrorMessage: "ERROR: Bad Request",
	},
	404: {
		ErrorNum:     404,
		ErrorMessage: "ERROR: Page Not Found",
	},
	500: {
		ErrorNum:     500,
		ErrorMessage: "ERROR: Internal Server Error",
	},
}

// ErrorPage function executes the error.html template with the given error code
func ErrorPg(wr io.Writer, error_code int) {

	fmt.Println("error code", error_code)
	err := Tmpl.ExecuteTemplate(wr, "header.html", nil)
	if err != nil {
		fmt.Println(err)
	}
	err = Tmpl.ExecuteTemplate(wr, "error.html", msg[error_code])
	if err != nil {
		fmt.Println(err)
	}
}
