package pages

import (
	"net/http"
)

func AddPostHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" && r.URL.Path == "/add_post" {
		err := Tmpl.ExecuteTemplate(w, "header_loggedin.html", nil)
		if err != nil {
			ErrorPg(w, 500)
		}
		Tmpl.ExecuteTemplate(w, "addpost.html", nil)
		if err != nil {
			ErrorPg(w, 500)
		}
	}

}