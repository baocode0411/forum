package pages

import (
	"fmt"
	"forum/data"
	"forum/internal/sql"
	"forum/internal/utility"
	"net/http"
	"time"

	"golang.org/x/crypto/bcrypt"
)

type RegistrationData struct {
	NameErr  string
	EmailErr string
	Username string
	Email    string
}

func HashPassword(password string) string {
	byt, err := bcrypt.GenerateFromPassword([]byte(password), 12)
	if err != nil {
		fmt.Println("Could not generate password", err.Error())
	}
	return string(byt)
}

func RegistrationHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {

		err := Tmpl.ExecuteTemplate(w, "registration.html", nil)
		if err != nil {
			ErrorPg(w, 500)
		}
	}
	if r.Method == "POST" {
		regData := data.User{}
		Error := RegistrationData{}
		regData.UserName = r.FormValue("username")
		regData.Email = r.FormValue("email")
		regData.Password = HashPassword(r.FormValue("password"))
		regData.Joined = time.Now().Format("2006-01-02 15:04:05")
		fmt.Println(regData.UserName)
		fmt.Println(regData.Email)
		fmt.Println(regData.Password)
		fmt.Println(regData.Joined)

		if sql.GetUser(regData.UserName, "name") != nil {
			Error.NameErr = "name taken, try being original for once."
		}

		if sql.GetUser(regData.Email, "email") != nil {
			Error.EmailErr = "email already in use. skill issue"
		}
		fmt.Println("Got here: ", Error)
		if Error.NameErr == "" && Error.EmailErr == "" {
			//sqlite stuff update and redirect to main page
			//registerUserToTable(regData.UserName, regData.Password, regData.Email)
			sql.AddUser(regData)
			data.AlertStruct.AlertCode = 200
			data.AlertStruct.AlertMessage = "Successfully registered account."
			utility.RedirectToMainPage(w, r)

		} else {
			//give error
			err := Tmpl.ExecuteTemplate(w, "registration.html", Error)
			if err != nil {
				fmt.Println(err)
				ErrorPg(w, 500)
			}
			//log.Fatal(regData.Error)
		}
	}
}
