package pages

import (
	"fmt"
	"forum/data"
	"forum/internal/sql"
	"forum/internal/utility"
	"net/http"
)

func AllPostHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("method for allposts", r.Method)
	fmt.Println(r.URL.Path)
	if r.URL.Path == "/all_posts" && r.Method == "GET" {
		allposts := data.AllPosts{}
		allposts.Loggedin = false
		if utility.CheckCookieExist(w, r) {
			allposts.Loggedin = true
			c, _ := r.Cookie("session_token")
			u := sql.GetUser(c.Value, "sess")

			if u == nil {
				//TODO change later
				fmt.Println("Could not get user by session")
			} else {
				allposts.UserName = u.UserName
			}
		}
		allposts.Posts = sql.GetAllPosts()
		allposts.Categories = sql.GetAllCategories()
		allposts.ErrorMessage = ""

		if allposts.Loggedin {
			err := Tmpl.ExecuteTemplate(w, "header_loggedin.html", nil)
			if err != nil {
				ErrorPg(w, 500)
			}
		} else {
			err := Tmpl.ExecuteTemplate(w, "header.html", allposts)
			if err != nil {
				fmt.Println("header.html")
				ErrorPg(w, 500)
			}
		}

		err := Tmpl.ExecuteTemplate(w, "allPosts.html", allposts)
		if err != nil {
			fmt.Println("allPosts.html")
			ErrorPg(w, 500)
		}
	}
	// if r.URL.Path != "/all_posts" && r.Method == "GET" {
	// 	fmt.Println("got to single post")
	// 	SinglePost(w, r)

	// }
}