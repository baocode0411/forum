package pages

import (
	"fmt"
	"forum/data"
	"forum/internal/sql"
	"forum/internal/utility"
	"net/http"

	"golang.org/x/crypto/bcrypt"
)

type LoginData struct {
	LoginSuccess string
	LoginErr     string
	PassErr      string
	Login        bool
}

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	loginData := LoginData{}
	if r.Method == "GET" {
		if utility.CheckCookieExist(w, r) {
			//redirect to mainpage or simply write you are currently logged in on another page.
			fmt.Println("cookies exist")
			loginData.LoginErr = "already logged in."
			loginData.Login = false
			//Tmpl.ExecuteTemplate(w, "header_loggedin.html", loginData)
			// tmpl.ExecuteTemplate(w, "main.html", loginData)
			utility.RedirectToPreviousPage(w, r)
		}
	}
	if r.Method == "POST" {
		userLogin := r.FormValue("userName")
		//password := HashPassword(r.FormValue("password"))
		password := r.FormValue("password")
		fmt.Println("userName", userLogin)
		fmt.Println("password", password)

		u := getUser(userLogin)

		if u == nil {
			loginData.LoginErr = "account does not exist"
			loginData.Login = false
		} else {
			userPass := u.Password
			fmt.Println("userpass", userPass)
			if bcrypt.CompareHashAndPassword([]byte(userPass), []byte(password)) == nil {
				fmt.Println("login Success")
				userKey := u.Id
				fmt.Println("userkey", userKey)
				utility.CreateCookie(r, w, userKey)
				loginData.Login = true
				data.AlertStruct.AlertCode = 202
				data.AlertStruct.AlertMessage = "Successfully logged in. We're all really proud of you."
				loginData.LoginSuccess = ""
				utility.RedirectToMainPage(w, r)
			} else {
				err := bcrypt.CompareHashAndPassword([]byte(userPass), []byte(password))
				if err != nil {
					fmt.Println("Password compare failed:", err.Error())
				}
				fmt.Print(err)
				fmt.Println("login unsuccessful", password, userPass)
				loginData.PassErr = "Password incorrect. Alzheimers?"
				loginData.Login = false
			}
		}
	}
	err := Tmpl.ExecuteTemplate(w, "login.html", loginData)
	if err != nil {
		ErrorPg(w, 500)
	}
}

func getUser(val string) *data.User {
	u := sql.GetUser(val, "name")

	if u == nil {
		u = sql.GetUser(val, "email")
	}

	return u
}
