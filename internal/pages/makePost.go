package pages

import (
	"fmt"
	"forum/data"
	"forum/internal/sql"
	"forum/internal/utility"
	"net/http"
	"strings"
	"time"
)

type MakePost struct {
	Categories     []data.Topic
	TitleError     string
	ContentError   string
	CategoryError  string
	PostSuccess    bool
	SuccessMessage string
}

// Gathers data from htmlform, processes it, places it in a struct and then sends it to the posts table.
func MakePostHandler(w http.ResponseWriter, r *http.Request) {
	postError := MakePost{}
	postError.Categories = sql.GetAllCategories()

	if !utility.CheckCookieExist(w, r) {
		utility.RedirectToMainPage(w, r)
		return
	}

	if r.Method == "POST" {
		postInfo := data.Post{}

		u, err := utility.GetUserIfLoggedIn(w, r)

		if err != nil {
			ErrorPg(w, 400)
			fmt.Println("Could not get user: ", err)
			return
		} else {
			postInfo.Creator = u.UserName
			postInfo.UserId = u.Id
		}
		postError.PostSuccess = true
		postInfo.Dislikes = 0
		postInfo.Likes = 0

		err = r.ParseForm()
		if err != nil {
			ErrorPg(w, 500)
			return
		}
		postInfo.Created = time.Now().Format(time.ANSIC)
		postInfo.Content = r.Form.Get("content")
		postInfo.Label = r.Form.Get("title")
		var categorySlice []string
		var combinedCatId = 0
		cat := 0
		for _, value := range r.Form["category"] {
			category := sql.GetTopic(value, "topic")
			if category == nil {
				postError.CategoryError = "Incorrect Category. How did you manage to fuck up this badly?"
				postError.PostSuccess = false
			} else {
				categorySlice = append(categorySlice, value)
				combinedCatId = combinedCatId + category.Id
				cat = cat + 1
			}

		}
		if cat == 0 {
			postError.CategoryError = "Select a category"
			postError.PostSuccess = false
		}
		if postError.PostSuccess {
			postInfo.Topic = strings.Join(categorySlice, ";")
			postInfo.TopicId = combinedCatId
		}
		foundLabel := utility.CheckInfoCorrect(postInfo.Label)
		foundContent := utility.CheckInfoCorrect(postInfo.Content)
		if postInfo.Label == "" || !foundLabel {
			postError.TitleError = "Post contains no title. Are you sure you are old enough/smart enough to use the internet?"
			postError.PostSuccess = false
		}
		if postInfo.Content == "" || !foundContent {
			postError.ContentError = "Post lacks content. Please click the red 'x' on the top of your screen to resolve this issue"
			postError.PostSuccess = false
		}
		if postError.PostSuccess {
			postError.SuccessMessage = "post created"
			sql.AddPost(postInfo)
		}
	}
	err := Tmpl.ExecuteTemplate(w, "header_loggedin.html", nil)
	if err != nil {
		ErrorPg(w, 500)
	}

	err = Tmpl.ExecuteTemplate(w, "makePost.html", postError)
	if err != nil {
		ErrorPg(w, 500)
	}
}
