package pages

import (
	"forum/internal/utility"
	"net/http"
)

func LogoutHandler(w http.ResponseWriter, r *http.Request) {
	if utility.CheckCookieExist(w, r) {
		c := http.Cookie{
			Name:   "session_token",
			MaxAge: -1,
		}
		http.SetCookie(w, &c)
		utility.RedirectToMainPage(w, r)

	} else {
		utility.RedirectToMainPage(w, r)
		//could link to errorpage that states you are already logged out, or could simply do nothing
	}

}