package pages

import (
	"fmt"
	"forum/data"
	"forum/internal/sql"
	"forum/internal/utility"
	"html/template"
	"net/http"
)

type LoginCheck struct {
	Loggedin        bool
	LoggedInMessage string
}

var Tmpl *template.Template

func StartpageHandler(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path == "/" && r.Method == "GET" {
		logincheck := LoginCheck{}
		if utility.CheckCookieExist(w, r) {
			logincheck.Loggedin = true
			logincheck.LoggedInMessage = "you are logged in :)"

			u, err := utility.GetUserIfLoggedIn(w, r)
			if err == nil {
				data.AlertStruct.LoggedIn = true
				data.AlertStruct.AllUserPosts = sql.GetUserPosts(u.Id)
				data.AlertStruct.AllReactions = sql.GetUserLikedPosts(u.Id)
				err = Tmpl.ExecuteTemplate(w, "header_loggedin.html", nil)
				if err != nil {
					ErrorPg(w, 500)
				}
			} else {
				data.AlertStruct.LoggedIn = false
				err := Tmpl.ExecuteTemplate(w, "header.html", nil)
				if err != nil {
					ErrorPg(w, 500)
				}
			}

		} else {
			data.AlertStruct.LoggedIn = false
			err := Tmpl.ExecuteTemplate(w, "header.html", nil)
			if err != nil {
				ErrorPg(w, 500)
			}
		}
	} else if r.URL.Path == "/" && r.Method != "GET" {
		ErrorPg(w, 400)
	} else if r.URL.Path != "/" {
		ErrorPg(w, 404)
		fmt.Println(r.URL.Path)
	}

	err := Tmpl.ExecuteTemplate(w, "main.html", data.AlertStruct)
	if err != nil {
		fmt.Println(err)
		ErrorPg(w, 500)
	}
	data.AlertStruct.AlertCode = 200
	data.AlertStruct.AlertMessage = ""
	data.AlertStruct.AllUserPosts = nil
	data.AlertStruct.AllReactions = nil
}