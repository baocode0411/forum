package utility

import (
	"forum/data"
	"forum/internal/sql"
	"net/http"
	"time"

	"github.com/gofrs/uuid"
)

func CheckCookieExist(w http.ResponseWriter, r *http.Request) bool {
	_, err := r.Cookie("session_token")

	return err != http.ErrNoCookie

}

func CreateCookie(r *http.Request, w http.ResponseWriter, userId int) {
	sessionToken := uuid.Must(uuid.NewV4()).String()

	http.SetCookie(w, &http.Cookie{
		Name:    "session_token",
		Value:   sessionToken,
		Path:    "/",
		Expires: time.Now().Add(1000 * time.Second),
	})

	sql.UpdateUser(data.User{Id: userId, Session: sessionToken}, true)
}
