package utility

import (
	"errors"
	"fmt"
	"forum/data"
	"forum/internal/sql"
	"net/http"
)

func RedirectToPreviousPage(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Referer", r.Header.Get("Referer"))
	http.Redirect(w, r, r.Header.Get("Referer"), http.StatusFound)
}
func RedirectToMainPage(w http.ResponseWriter, r *http.Request) {
	//TODO: add alert messages
	http.Redirect(w, r, "/", http.StatusFound)
}

func GetUserIfLoggedIn(w http.ResponseWriter, r *http.Request) (*data.User, error) {
	c, err := r.Cookie("session_token")

	if err != nil {
		fmt.Println("Could not retrieve cookie:", err.Error())

		return nil, errors.New("you need to be logged in dumbass")
	}

	u := sql.GetUser(c.Value, "sess")
	if u == nil {
		c := http.Cookie{
			Name:   "session_token",
			MaxAge: -1,
			Path:   "/",
		}
		http.SetCookie(w, &c)
		return nil, errors.New("user does not exist. Stop fucking around")
	}
	return u, nil
}

func CheckInfoCorrect(info string) bool {
	foundCharacter := false
	for _, r := range info {
		if r != ' ' && r != '\t' && r != '\r' {
			foundCharacter = true
		}
	}
	return foundCharacter
}
