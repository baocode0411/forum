#!/bin/sh
docker container prune
docker image prune
docker image build -f Dockerfile -t forum-docker-img .
docker images
while docker run -p 8080:8080 --detach --name forum-docker-cont forum-docker-img
do
 docker ps -a
 docker exec -it forum-docker-cont /bin/sh
done
docker stop forum-docker-cont
docker container rm forum-docker-cont
docker rmi forum-docker-img
# Too intrusive as it stops and deletes ALL containers and images.
# docker stop $(docker ps -aq)
# docker container rm $(docker container ls -aq)
# docker rmi $(docker images -q)