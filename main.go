package main

import (
	"fmt"
	"forum/internal/pages"
	"forum/internal/sql"
	"html/template"
	"log"
	"net/http"
	"os"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	sql.InitDatabase()
	initTemp()

	defer sql.DB.Close()

	css := http.FileServer(http.Dir("templates/css/"))
	http.Handle("/css/", http.StripPrefix("/css/", css))

	js := http.FileServer(http.Dir("js"))
	http.Handle("/js/", http.StripPrefix("/templates/js/", js))

	http.HandleFunc("/register", pages.RegistrationHandler)
	http.HandleFunc("/login", pages.LoginHandler)
	http.HandleFunc("/add_post", pages.AddPostHandler)
	http.HandleFunc("/make_post", pages.MakePostHandler)
	http.HandleFunc("/logout", pages.LogoutHandler)
	http.HandleFunc("/", pages.StartpageHandler)
	http.HandleFunc("/all_posts", pages.AllPostHandler)
	http.HandleFunc("/all_posts/", pages.SinglePost)
	//http.HandelFunc("/error", pages.ErrorHandler)

	fmt.Println("Server is listening to port 8080...")
	go exitProgram()
	if http.ListenAndServe(":8080", nil) != nil {
		log.Fatalf("%v - Internal Server Error", http.StatusInternalServerError)
	}

}

func exitProgram() {
	xpressed := ""
	fmt.Println("Type 'x' to quit.")
	fmt.Scan(&xpressed)
	if xpressed == "x" {
		os.Exit(0)
	} else {
		exitProgram()
	}
}

func initTemp() {
	pages.Tmpl = template.Must(template.ParseGlob("templates/*.html"))
	if pages.Tmpl == nil {
		println("template is nil")
	}
}
